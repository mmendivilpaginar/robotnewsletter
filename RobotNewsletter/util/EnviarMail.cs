﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Mail;
using System.Configuration;


namespace RobotNewsletter.util
{
    public class EnviarMail
    {

        bool MODO;
        string MailToTest;
        string _from;
        string _to;
        string _cc;
        string _subjet;
        string _body;
        string _smtp;
        string _displayname;
        string _port;
        string _pass;

        public string DISPLAYNAME
        {
            get { return _displayname; }
            set { _displayname = value; }
        }
        public string FROM
        {
            get { return _from; }
            set { _from = value; }

        }
        public string TO
        {
            get { return _to; }
            set { _to = value; }
        }
        public string CC
        {
            get { return TO; }
            set { TO = value; }
        }
        public string SUBJECT
        {
            get { return _subjet; }
            set { _subjet = value; }
        }
        public string BODY
        {
            get { return _body; }
            set { _body = value; }
        }

        public string SMTP
        {
            get { return _smtp; }
            set { _smtp = value; }
        }

        public string Port
        {
            get { return _port; }
            set { _port = value; }
        }

        public string Pass
        {
            get { return _pass; }
            set { _pass = value; }
        }


        public void getConfiguracion()
        {
            SMTP = ConfigurationManager.AppSettings["SMTP"];
            MODO = bool.Parse(ConfigurationSettings.AppSettings["DEBUG_MODE"].ToString());
            MailToTest = ConfigurationSettings.AppSettings["MAILTOTEST"].ToString();            
            SMTP = ConfigurationSettings.AppSettings["smtp"];
            Port = ConfigurationSettings.AppSettings["port"];
            FROM = ConfigurationSettings.AppSettings["from"];
            Pass = ConfigurationSettings.AppSettings["Password"];
            
        }


        public string Enviar(string FROM,string TO, string SUBJECT, string DisplayName, AlternateView htmlView)
        {
            getConfiguracion();

            System.Net.Mail.MailMessage msg = new System.Net.Mail.MailMessage();
            if(MODO)
                msg.To.Add(MailToTest);
            else
            msg.To.Add(TO);

            msg.From = new MailAddress(FROM, DisplayName);
            msg.Subject = SUBJECT;
            msg.AlternateViews.Add(htmlView);

            string enviado;
            try
            {


                SmtpClient client = new SmtpClient(SMTP);
                client.Port = int.Parse(Port);

                if (!((FROM == null) && (Pass == null)))
                {
                    client.Credentials = new System.Net.NetworkCredential(FROM, Pass);

                }



                client.EnableSsl = false;
                client.Send(msg);
                client = null;
                msg.Dispose();
                enviado = "true";



            }
            catch (Exception ex)
            {
                enviado = ex.Message;
                //enviado = false;
            }

            return enviado;

        }

    }
}
