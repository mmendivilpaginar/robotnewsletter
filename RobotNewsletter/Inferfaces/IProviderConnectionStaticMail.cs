﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RobotNewsletter.Inferfaces
{
    public interface IProviderConnectionStaticMail
    {
        string Port { get; }
        Boolean EnableSsl { get; }
        string User { get; }
        string Password { get; }
        string SMTP_Address { get; }
        string From { get; }
        string FromName { get; }
        string SiteUrl { get; }

    }
}
