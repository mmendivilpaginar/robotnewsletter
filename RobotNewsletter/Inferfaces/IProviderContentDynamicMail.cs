﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RobotNewsletter.Inferfaces
{
    public interface IProviderContentDynamicMail
    {
        string Body { get; }
        string[] To { get; }

        string Asunto { get; }
        Boolean IsHtmlBody { get; }



    }
}
